import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        String[] employeeNames = {"Alice Dupont", "Bob Martin", "Charlie Besson", "Diane Loriot", "Eva Joly"};
        int[] hoursWorked = {35, 38, 35, 38, 40};
        double[] hourlyRates = {12.5, 15.0, 13.5, 14.5, 13.0};
        String[] positions = {"Caissier", "Projectionniste", "Caissier", "Manager", "Caissier"};

        for (int i = 0; i < employeeNames.length; i++) {
            double salary;
            if (hoursWorked[i] > 35) {
                salary = 35 * hourlyRates[i] + (hoursWorked[i] - 35) * hourlyRates[i] * 1.5;
            } else {
                salary = hoursWorked[i] * hourlyRates[i];
            }
            System.out.println("Salaire de " + employeeNames[i] + " : " + salary + " €");
        }

        ArrayList<String> findEmployees = new ArrayList<>();
        String employee = "Caissier";

        for(int i = 0; i < employeeNames.length; i++){
            if(positions[i].equals(employee)){
                findEmployees.add(employeeNames[i]);
            }
        }
        if (!findEmployees.isEmpty()){
            System.out.println(findEmployees);
        } else {
            System.out.println("Aucun résultat");
        }
    }

}